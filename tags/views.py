from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from tags.models import Tag
from recipes.models import ShoppingItem

# Create your views here.


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"
    paginate_by = 2

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class TagCreateView(LoginRequiredMixin, CreateView):
    model = Tag
    template_name = "tags/new.html"
    fields = ["name"]
    success_url = reverse_lazy("tags_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class TagUpdateView(LoginRequiredMixin, UpdateView):
    model = Tag
    template_name = "tags/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("tags_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class TagDeleteView(LoginRequiredMixin, DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context
