from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from recipes.forms import RatingForm, ShoppingItemForm


# from recipes.forms import RecipeForm
from recipes.models import Recipe, ShoppingItem, Ingredient

from django.views.decorators.http import require_http_methods


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        if self.request.user.is_authenticated:
            shopping_food_list = []
            for item in ShoppingItem.objects.filter(user=self.request.user):
                shopping_food_list.append(item.food_item)
            context["user_shopping_list"] = shopping_food_list
            context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "recipes/shopping_list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total_item_shop"] = len(ShoppingItem.objects.filter(user=self.request.user))
        return context


@require_http_methods(['POST'])
def add_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(pk=ingredient_id)
    ShoppingItem.objects.create(user=request.user, food_item=ingredient.food)
    return redirect("recipe_detail", pk=ingredient.recipe.id)


@require_http_methods(['POST'])
def delete_shopping_list(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_list")
